# ido
ido桌面工具，自用版，集合多种常用工具，打开一个界面就满足所需
# 这是Vue编写的界面，需同时下载https://gitee.com/lidiaolong/dock_service.git 服务端配合
# 已有功能
* 导航模块
* * 添加自己常用的导航，不用再一个浏览器一书签了
![控制台](http://other.ldldaily.cn/dock-%E5%AF%BC%E8%88%AA.png "控制台")
* 日历模块
* * 添加自己的特殊日子在日历中显示，在特定日子给予提示
![控制台](http://other.ldldaily.cn/dock-%E6%97%A5%E5%8E%86%E6%8F%90%E9%86%92.png "控制台")
* 提醒模块
* * 可以设置提醒，到达设定时间会有声音提醒，如每30分钟提醒喝水
![控制台](http://other.ldldaily.cn/dock-%E5%AE%9A%E6%97%B6%E6%8F%90%E9%86%92.png "控制台")
* 脚本模块（个人是用mac的所以多是按mac操作）
* * 可以编辑常用命令，之后一键即可执行命令
![控制台](http://other.ldldaily.cn/dock-%E8%84%9A%E6%9C%AC.png "控制台")
* 博客模块
* * 可以做记事本使用
![控制台](http://other.ldldaily.cn/dock-%E5%8D%9A%E5%AE%A2.png "控制台")
* redis模块
* * 可以管理当前电脑的redis
![控制台](http://other.ldldaily.cn/dock-redis.png "控制台")
* 接口并发测试模块
* * 可以自定义并发接口请求测试，可查看请求日志
![控制台](http://other.ldldaily.cn/dock-%E6%8E%A5%E5%8F%A3%E6%B5%8B%E8%AF%95.png "控制台")
* 日志模块
* * 添加本地日志文件，快速查看
![控制台](http://other.ldldaily.cn/dock-%E6%97%A5%E5%BF%97.png "控制台")
* 常用工具箱模块
* * 计算器
![控制台](http://other.ldldaily.cn/dock-%E8%AE%A1%E7%AE%97%E5%99%A8.png "控制台")
* * 时间戳转换
![控制台](http://other.ldldaily.cn/dock-%E6%97%B6%E9%97%B4%E6%88%B3.png "控制台")
* * 文件互传
![控制台](http://other.ldldaily.cn/dock-%E6%96%87%E4%BB%B6%E4%BA%92%E4%BC%A0.png "控制台")
## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
