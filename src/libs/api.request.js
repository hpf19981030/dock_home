import HttpRequest from '@/libs/axios'
import config from '@/config'
// 获取域名
var httpHostNameByApiRequset = ''
if (config.setBaseHostFromHost) {
  // 取主机名
  httpHostNameByApiRequset = 'http://' + config.baseUserSetHost + config.serverPort
} else {
  // 取用户自定义
  httpHostNameByApiRequset = process.env.NODE_ENV === 'development' ? config.baseUrl.dev : config.baseUrl.pro
}
const baseUrl = httpHostNameByApiRequset
// 创建
const axios = new HttpRequest(baseUrl)
export default axios
