/*
createNotification({
  title: '标题',
  options: {
    body: '消息内容'
  },
  duration: 3000
})
*/
export default {
  createNotification(data) {
    return createNotify(data)
  }
}

/*
* @param {string} title - 消息标题
* @param {object} options - 消息配置
* @param {number} duration - 消息时长
* @param {function} onclick
* @param {function} onshow
* @param {function} onclose
* @param {function} onerror
* @return {object}
*/
async function createNotify(data) {
  // 一些判断
  if (window.Notification) {
    if (Notification.permission === 'granted') {
      return notify(data)
    } else if (Notification.permission === 'denied') {
      console.log('用户拒绝通知')
    } else if (Notification.permission === 'default') {
      const [err, status] = await Notification.requestPermission().then(status => [null, status]).catch(err => [err, null])
      return err || status === 'denied' ? Promise.reject() : notify(data)
    }
  }
  // 构造实例
  function notify(data) {
    const { title, options, duration } = data
    options.requireInteraction = true
    const notification = new Notification(title, options)
    setTimeout(() => notification.close(), duration || 5000)
    // 绑定事件
    const methods = ['onclick', 'onshow', 'onclose', 'onerror']
    for (let i = 0; i < methods.length; i++) {
      const method = methods[i]
      if (typeof options[method] === 'function') {
        notification[method] = options[method]
      }
    }
    return notification
  }
  return Promise.reject()
}
