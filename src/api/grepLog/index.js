import axios from '@/libs/api.request'

export const GetGrepLogList = (params) => {
  return axios.request({
    url: '/api/greplog/GetGrepLogList',
    method: 'get',
    params
  })
}

export const GetGrepLog = (params) => {
  return axios.request({
    url: '/api/greplog/GetGrepLog',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const OperationGrepLog = (params) => {
  return axios.request({
    url: '/api/greplog/OperationGrepLog',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}
