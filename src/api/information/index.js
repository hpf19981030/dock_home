import axios from '@/libs/api.request'

export const GetInformation = (params) => {
  return axios.request({
    url: '/api/information/GetInformation',
    method: 'get',
    params
  })
}

export const GetInformationList = (params) => {
  return axios.request({
    url: '/api/information/GetInformationList',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const OperationInformation = (params) => {
  return axios.request({
    url: '/api/information/OperationInformation',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}
