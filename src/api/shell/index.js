import axios from '@/libs/api.request'

export const GetShellList = (params) => {
  return axios.request({
    url: '/api/shell/GetShellList',
    method: 'get',
    params
  })
}

export const OperationShell = (params) => {
  return axios.request({
    url: '/api/shell/OperationShell',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const RunShell = (params) => {
  return axios.request({
    url: '/api/shell/RunShell',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const GetShellSortList = (params) => {
  return axios.request({
    url: '/api/shell/GetShellSortList',
    method: 'get',
    params
  })
}

export const OperationShellSort = (params) => {
  return axios.request({
    url: '/api/shell/OperationShellSort',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const GetShellListByType = (params) => {
  return axios.request({
    url: '/api/shell/GetShellListByType',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}
