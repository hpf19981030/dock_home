import axios from '@/libs/api.request'

export const GetBlogList = (params) => {
  return axios.request({
    url: '/api/blog/GetBlogList',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const OperationBlog = (params) => {
  return axios.request({
    url: '/api/blog/OperationBlog',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const GetBlogInfo = (params) => {
  return axios.request({
    url: '/api/blog/GetBlogInfo',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}
