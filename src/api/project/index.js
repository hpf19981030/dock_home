import axios from '@/libs/api.request'

export const GetProjectList = (params) => {
  return axios.request({
    url: '/api/project/GetProjectList',
    method: 'get',
    params
  })
}

export const OperationProject = (params) => {
  return axios.request({
    url: '/api/project/OperationProject',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const OperationProjectInterface = (params) => {
  return axios.request({
    url: '/api/project/OperationProjectInterface',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const GetProjectInterfaceList = (params) => {
  return axios.request({
    url: '/api/project/GetProjectInterfaceList',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const RunProject = (params) => {
  return axios.request({
    url: '/api/project/RunProject',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const GetProjectInterfaceLog = (params) => {
  return axios.request({
    url: '/api/project/GetProjectInterfaceLog',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}
