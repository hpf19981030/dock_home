import axios from '@/libs/api.request'

export const GetSpecialDayList = (params) => {
  return axios.request({
    url: '/api/user/GetSpecialDayList',
    method: 'get',
    params
  })
}

export const OperationSpecialDay = (params) => {
  return axios.request({
    url: '/api/user/OperationSpecialDay',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const GetRemindList = (params) => {
  return axios.request({
    url: '/api/user/GetRemindList',
    method: 'get',
    params
  })
}

export const OperationRemind = (params) => {
  return axios.request({
    url: '/api/user/OperationRemind',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}
