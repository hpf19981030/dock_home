import axios from '@/libs/api.request'

export const Calculator = (params) => {
  // 计算器
  return axios.request({
    url: '/api/index/Calculator',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const TimeTransform = (params) => {
  // 时间转换
  return axios.request({
    url: '/api/index/TimeTransform',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const GetSearchKeywordPage = (params) => {
  // 搜索记录
  return axios.request({
    url: '/api/index/GetSearchKeywordPage',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const AddSearchKeyword = (params) => {
  // 添加搜索记录
  return axios.request({
    url: '/api/index/AddSearchKeyword',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const DelSearchKeyword = (params) => {
  // 删除搜索记录
  return axios.request({
    url: '/api/index/DelSearchKeyword',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}
