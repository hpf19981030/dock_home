import axios from '@/libs/api.request'

export const RedisKeys = (params) => {
  return axios.request({
    url: '/api/index/RedisKeys',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const RedisValue = (params) => {
  return axios.request({
    url: '/api/index/RedisValue',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const RedisDel = (params) => {
  return axios.request({
    url: '/api/index/RedisDel',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}
