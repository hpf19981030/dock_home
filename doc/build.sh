#!/bin/bash

echo "开始打包发布。。。"

FilePath="/Users/ido/Go/dock_home"
FromPath="/Users/ido/Go/dock/dist"

cd $FilePath
echo "当前操作目录：$FilePath";

echo "开始打包。。。";
npm run build
echo "打包完成。";

echo "开始清除旧运行空间。。。";
sudo rm -rf $FromPath/*
echo "清除完成。。";

echo "开始移动打包数据。。。";
sudo mv $FilePath/dist/* $FromPath
echo "移动完成。";

echo "打包发布结束，运行空间："
echo "cd $FromPath"
open $FromPath
